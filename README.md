# Alfresco Share JAR Module - SDK 3

To run this module use `mvn clean install -DskipTests=true alfresco:run` or `./run.sh` and verify that it 

 * Runs the embedded Tomcat + H2 DB 
 * Runs Alfresco Share
 * Packages both as JAR and AMP assembly

Note. You access Share as follows: http://localhost:8081/share
 
Note. You need an Alfresco Platform instance running at http://localhost:8080/alfresco that Share can talk to.
      Typically you will just kick off a platform-jar module for that.
 
# Few things to notice

 * No parent pom
 * WAR assembly is handled by the Alfresco Maven Plugin configuration, if needed
 * Standard JAR packaging and layout
 * Works seamlessly with Eclipse and IntelliJ IDEA
 * JRebel for hot reloading, JRebel maven plugin for generating rebel.xml, agent usage: `MAVEN_OPTS=-Xms256m -Xmx1G -agentpath:/home/martin/apps/jrebel/lib/libjrebel64.so`
 * AMP as an assembly
 * [Configurable Run mojo](https://github.com/Alfresco/alfresco-sdk/blob/sdk-3.0/plugins/alfresco-maven-plugin/src/main/java/org/alfresco/maven/plugin/RunMojo.java) in the `alfresco-maven-plugin`
 * No unit testing/functional tests just yet
 * Resources loaded from META-INF
 * Web Fragment (this includes a sample servlet configured via web fragment)
 

 **Alfresco search text highlight**
```
Alfresco have functionality to inline search text from within the repository documents and search the text from a document and  highlight the all available text from within the document .

So for that you need to first search your document and you have to make manually search for the required text in the document .

What we have implemented is that you have to search for the required document and list of results will appear and after clicking on the document , document details page will get open and the all searched text will get highlighted within the document previewer.

This will remove your extra required click and enter search text again.

Click on search results and document details page will get open and will highlight the document previewer within available document text.```

   
  
 
